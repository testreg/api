<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
	public function authorize(): bool
	{
		return true;
	}


	public function rules(): array
	{
		return [
			'title' => 'required|string|max:255',
			'review' => 'required|string',
		];
	}


	public function attributes(): array
	{
		return [
			'title' => 'Заголовок',
			'review' => 'Описание',
		];
	}


	protected function validationData(): array
	{
		$data = $this->all();
		$data['title'] = strip_tags($this->input('title'));

		return $data;
	}
}
