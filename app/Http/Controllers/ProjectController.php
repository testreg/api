<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectRequest;
use App\Project;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{

	/**
	 * Display a listing of the resource
	 *
	 * @return JsonResponse
	 */
	public function index()
	{
		$projects = Project::query()->where('user_id', Auth::id())->get();

		return response()->json([
			'status' => 'success',
			'data' => [
				'projects' => $projects->keyBy('id'),
			]
		], 200);
	}

	/**
	 * Store a newly created resource in storage
	 *
	 * @param ProjectRequest $request
	 * @return JsonResponse
	 */
	public function store(ProjectRequest $request)
	{
		$project = new Project();
		$project->fill($request->validated());
		$project->user_id = Auth::id();

		if (!$project->save()) {
			return response()->json([
				'status' => 'error',
				'message' => 'Project is not created',
			], 503);
		}

		return response()->json(['status' => 'success'], 201);
	}

	/**
	 * Update the specified resource in storage
	 *
	 * @param ProjectRequest $request
	 * @param int $id
	 * @return JsonResponse
	 */
	public function update(ProjectRequest $request, int $id)
	{
		$project = Project::query()
		                  ->where('user_id', Auth::id())
		                  ->find($id);

		if (!$project){
			return response()->json([
				'status' => 'error',
				'message' => 'There is no such project',
			], 410);
		}

		$project->fill($request->validated());
		$project->update();

		return response()->json(['status' => 'success'], 200);
	}

	/**
	 * Remove the specified resource from storage
	 *
	 * @param int $id
	 * @return JsonResponse
	 * @throws Exception
	 */
	public function destroy(int $id)
	{
		$project = Project::query()
		                  ->select('id')
		                  ->where('user_id', Auth::id())
		                  ->find($id);

		if (!$project){
			return response()->json([
				'status' => 'error',
				'message' => 'There is no such project',
			], 410);
		}

		$project->delete();

		return response()->json(['status' => 'success'], 200);
	}
}
