<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class TokenController extends Controller
{
	/**
	 * Token refreshing
	 *
	 * @return JsonResponse
	 */
	public function update()
	{
		$token = self::updateToken();

		return response()->json([
			'status' => 'success',
			'message' => 'Your token successfully refreshed',
			'data' => [
				'token' => $token,
			]
		], 200);
	}

	/**
	 * New token generating and saving
	 *
	 * @return string
	 */
	public static function updateToken()
	{
		$user = Auth::user();
		$token = Str::random(60);
		$user->forceFill([
			'api_token' => hash('sha256', $token),
		])->save();

		return $token;
	}

}
