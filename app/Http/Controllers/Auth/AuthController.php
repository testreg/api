<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
	/**
	 * Authentication and authorisation
	 *
	 * @param AuthRequest $request
	 * @param TokenController $token_manager
	 * @return JsonResponse
	 */
	public function login(AuthRequest $request, TokenController $token_manager)
	{
		$credentials = $request->only('email', 'password');

		if (Auth::once($credentials)) {

			$token = $token_manager::updateToken();

			return response()->json([
				'status' => 'success',
				'message' => 'You are successfully logged in',
				'data' => [
					'token' => $token,
				]
			], 200);
		}

		return response()->json([
			'status' => 'auth_error',
			'message' => 'You can not sign with those credentials'
		], 403);
	}
}
