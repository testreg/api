<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if ($this->tokenIsNotValid()) {
		    return response()->json([
			    'status' => 'unauthenticated',
			    'message' => 'Sorry. Your token is missing or out of date. Access denied'
		    ], 403);
	    }

        return $next($request);
    }

    private function tokenIsNotValid(){
    	return ! Auth::guard('api')->check();
    }
}
