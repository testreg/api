<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DenyIfTokenAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    if (Auth::guard('api')->check()){

		    // Клиент, пришедший с валидным токеном (авторизован)
		    // не должен запускать процесс аутентификации
		    return response()->json( [
			    'status' => 'already_logged_in',
			    'message' => 'Sorry. You are already logged in. Access denied'
		    ], 403);
	    }

        return $next($request);
    }
}
