<?php

use Illuminate\Database\Seeder;

class TestUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DB::table('users')->insert([
		    'name' => 'admin',
		    'email' => 'john@exapmle.com',
		    'password' => '$2y$10$dxF3g2SP9.XODtpxxz0UDeYmUq7l./ysGETYY2RJXefwmVTeP/f/m', // password
	    ]);
	    DB::table('users')->insert([
		    'name' => 'user',
		    'email' => 'mike@exapmle.com',
		    'password' => '$2y$10$dxF3g2SP9.XODtpxxz0UDeYmUq7l./ysGETYY2RJXefwmVTeP/f/m', // password
	    ]);
    }
}
