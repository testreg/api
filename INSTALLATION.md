## Installation

1. Clone the project
2. Enter the project directory and execute: `composer install`
3. Create the .env file and set params to connect to your database
4. Execute `php artisan key:generate`
5. Execute `php artisan migrate`
6. If you want to create test users, execute `php artisan db:seed --class=TestUsersSeeder`
