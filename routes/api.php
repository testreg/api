<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('auth', 'Auth\AuthController@login')->middleware(['web.guard','guest'])->name('login');

Route::middleware(['api.auth'])->group( function() {
	Route::get('token', 'Auth\TokenController@update');
	Route::resource('projects', 'ProjectController')->except(['create', 'edit', 'show']);
});